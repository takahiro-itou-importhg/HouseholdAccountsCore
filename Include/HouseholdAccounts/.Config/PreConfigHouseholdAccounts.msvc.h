//  -*-  coding: utf-8; mode: c++  -*-  //
/*************************************************************************
**                                                                      **
**                  ---  Household Accounts Core.  ---                  **
**                                                                      **
**          Copyright (C), 2017-2017, Takahiro Itou                     **
**          All Rights Reserved.                                        **
**                                                                      **
*************************************************************************/

/**
**      スクリプトによる設定値が書き込まれるヘッダファイル。
**
**      @file       .Config/ConfiguredHouseholdAccounts.h.in
**/

#if !defined( HACCOUNTS_CONFIG_INCLUDED_CONFIGURED_HOUSEHOLD_ACCOUNTS_H )
#    define   HACCOUNTS_CONFIG_INCLUDED_CONFIGURED_HOUSEHOLD_ACCOUNTS_H

//========================================================================
//
//    Name Space.
//

/**
**    スクリプトによって設定された名前空間。
**/

#define     HOUSEHOLD_ACCOUNTS_CNF_NS               \
    HouseholdAccounts

/**
**    名前空間。
**/

#define     HOUSEHOLD_ACCOUNTS_NAMESPACE            \
    HOUSEHOLD_ACCOUNTS_CNF_NS

#define     HOUSEHOLD_ACCOUNTS_NAMESPACE_BEGIN      \
    namespace  HOUSEHOLD_ACCOUNTS_CNF_NS  {

#define     HOUSEHOLD_ACCOUNTS_NAMESPACE_END        \
    }


//========================================================================
//
//    Compile Features.
//

//
//    キーワード constexpr  の検査。
//

#if ( 1 )
#    define     HOUSEHOLD_ACCOUNTS_ENABLE_CONSTEXPR         1
#else
#    undef      HOUSEHOLD_ACCOUNTS_ENABLE_CONSTEXPR
#endif

#if !defined( CONSTEXPR_VAR ) && !defined( CONSTEXPR_FUNC )
#    if ( HOUSEHOLD_ACCOUNTS_ENABLE_CONSTEXPR )
#        define     CONSTEXPR_VAR       constexpr
#        define     CONSTEXPR_FUNC      constexpr
#    else
#        define     CONSTEXPR_VAR       const
#        define     CONSTEXPR_FUNC
#    endif
#endif

//
//    キーワード nullptr  の検査。
//

#if ( 1 )
#    define     HOUSEHOLD_ACCOUNTS_ENABLE_NULLPTR           1
#else
#    if !defined( nullptr )
#        define     nullptr     NULL
#    endif
#    undef      HOUSEHOLD_ACCOUNTS_ENABLE_NULLPTR
#endif

//
//    キーワード override の検査。
//

#if ( 1 )
#    define     HOUSEHOLD_ACCOUNTS_ENABLE_OVERRIDE          1
#else
#    if !defined( override )
#        define     override
#    endif
#    undef      HOUSEHOLD_ACCOUNTS_ENABLE_OVERRIDE
#endif

#endif
